﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Dialogs.DialogEditor;

public class DialogController : MonoBehaviour
{
  public GameObject[] Triggers;
  public GameObject DialogView;
  public GameObject CurrentTrigger;
  private Text dialogText;
  private bool closed;

  void Start()
  {
    dialogText = DialogView.GetComponentInChildren<Text>();
  }

  void Update()
  {
    checkTriggers();
  }

  public void checkTriggers()
  {
    if (getCurrentNode() == null) return;
    if (CurrentTrigger.active) get_message();
    checkNextButtonState();
  }

  public void set_message(string message)
  {
    dialogText.text = message;
  }

  void get_message()
  {
    dialogText.text = getCurrentNode().text;
  }

  private NodeSerialized getCurrentNode()
  {
    return getDialogAgent().CurrentNode;
  }

  void clear_message()
  {
    dialogText.text = "";
  }

  DialogAgent getDialogAgent()
  {
    return CurrentTrigger.GetComponent<DialogAgent>();
  }

  public void closeButton()
  {
    DialogView.SetActive(false);
    closed = true;
  }

  public void nextButton()
  {
    if (getCurrentNode() == null) return;
    nextLine();
  }

  public GameObject NextButtonObject;

  private void checkNextButtonState()
  {
    if (getCurrentNode().attachedNode > 0) NextButtonObject.active = true;
  }

  private void nextLine()
  {
    DialogAgent da = getDialogAgent();
    da.StartNodeId = getCurrentNode().attachedNode;
    if (da.StartNodeId > 0) dialogText.text = da.getText();
    else
    {
      da.dialogEnd();
      clear_message();
      NextButtonObject.active = false;
    }
  }
}