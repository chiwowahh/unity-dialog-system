﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DialogNode : EditorWindow
{
    private DialogEditor dialogEditor;
    public Rect WindowFrame;
    public int Id;
    public string Text = "";
    public int AttachedNode = -1;

    public DialogNode(DialogEditor _dialogEditor)
    {
        WindowFrame = new Rect(10, 10, 200, 200);
        dialogEditor = _dialogEditor;

        if (dialogEditor.Nodes.Count == 0)
        {
            Id = dialogEditor.Nodes.Count;
        }
        else
        {
            Id = dialogEditor.CurrentNodeId + 1;
        }
    }

    public void render()
    {
        WindowFrame = GUI.Window(Id, WindowFrame, DrawNodeWindow, "Line " + Id);
    }

    void DrawNodeWindow(int windowId)
    {
        DialogNode node = dialogEditor.Nodes[windowId] as DialogNode;
        GUILayout.BeginHorizontal("Buttons");
            string attached;
            if (node.AttachedNode > 0) attached = "attached to " + node.AttachedNode;
            else attached = "attach";

            if (GUILayout.Button(attached)) dialogEditor.WindowsToAttach.Add(windowId);
            if (GUILayout.Button("detach")) node.AttachedNode = -1;
            if (GUILayout.Button("delete")) dialogEditor.Nodes.Remove(windowId);
        GUILayout.EndHorizontal();

        GUILayout.BeginArea(new Rect(10,45,180,190));
            node.Text = GUILayout.TextArea(node.Text, 500 );
        GUILayout.EndArea();

        GUI.DragWindow(new Rect(0, 0, 10000, 20));
    }
}