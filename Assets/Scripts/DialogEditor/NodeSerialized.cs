﻿using System;

namespace Dialogs.DialogEditor
{
    [Serializable]
    public class NodeSerialized
    {
        public int id;
        public string text = "";
        public int attachedNode = -1;
    }
}