﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dialogs.DialogEditor;
using UnityEngine;

public class DialogAgent : MonoBehaviour
{
    public TextAsset DialogFile;
    public NodeSerialized CurrentNode;
    public int StartNodeId;
    public Hashtable Nodes = new Hashtable();
    private NodeSerialized nodesSerial;
    public object Trigger;
    void Start()
    {
        if (DialogFile != null)
        {
            string json = DialogFile.ToString();
            try
            {
                NodeSerialized[] nodesSerial = JsonHelper.FromJson<NodeSerialized>(json);
                Nodes = new Hashtable();
                foreach (var nodeSerial in nodesSerial)
                {
                    Nodes.Add(nodeSerial.id, nodeSerial);
                }
            }
            catch (Exception e)
            {
            }
        }
    }

    void Update()
    {
        if (Nodes.Count > 0) setCurrentNode(StartNodeId);
    }

    public string getText()
    {
        var currentNode = Nodes[StartNodeId] as NodeSerialized;
        if (currentNode == null) return "";
        return currentNode.text;
    }

    public void setCurrentNode(int id)
    {
        CurrentNode = Nodes[id] as NodeSerialized;
    }

    public void dialogEnd()
    {
        gameObject.active = false;
    }
}